class CreateCoins < ActiveRecord::Migration[5.2]
  def change
    create_table :coins do |t|
      t.integer :cmc_id,      null: false
      t.string :name,         null: false
      t.string :symbol,       null: false
      t.string :slug,         null: false
      t.string :logo
      t.string :website
      t.string :source_code
      t.string :reddit
      t.string :twitter

      t.timestamps
    end
  end
end
