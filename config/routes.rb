Rails.application.routes.draw do
  post 'auth/login', to: 'authentication#authenticate'
  post 'signup', to: 'users#create'

  get 'listings', to: 'coin_market_cap#listings'
  post 'get_metadata', to: 'coin_market_cap#get_metadata'
  post 'selected_coins_prices', to: 'coin_market_cap#selected_coins_prices'
end
