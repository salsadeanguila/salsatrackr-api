module CmcApi
  require 'rest-client'
  require 'digest'

  @@cache = ActiveSupport::Cache::MemoryStore.new

  def self.api_url
    if Rails.env.production?
      ENV["PROD_API_URL"]
    else
      ENV["DEV_API_URL"]
    end
  end

  def self.headers
    {
      'X-CMC_PRO_API_KEY' => ENV["CMC_API_KEY"]
    }
  end

  def self.listings
    url = "#{self.api_url}/cryptocurrency/listings/latest?limit=5000"
    begin
      response = JSON.parse(RestClient.get(url, self.headers))
    rescue RestClient::ExceptionWithResponse => e
      puts e.response
    end
    return response
  end

  def self.get_metadata(id, listings)
    url = "#{self.api_url}/cryptocurrency/info?id=#{id}"
    begin
      response = JSON.parse(RestClient.get(url, self.headers))
      if response["data"].length > 1
        return response
      end
      response["data"]["#{id}"]["metadata"] = Coin.find_by_cmc_id(response["data"].first)
      listings["data"].each do |listing|
        if id == listing["id"]
          response["data"]["#{id}"]["listing"] = listing
        end
      end
      return response
    rescue RestClient::ExceptionWithResponse => e
      puts e.response
    end
  end

  def self.get_all_metadata(listings)
    coins_ids_array = []
    cc = 0
    index = 0
    coins_ids_array[index] = []

    listings["data"].each do |listing|
      coins_ids_array[index].push(listing["id"])

      cc += 1
      if cc % 100 == 0
        index += 1
        coins_ids_array[index] = []
      end
    end

    coins_ids_array.map! {|array| array.to_s[1..-2].delete(" ")}

    # Iterates through each string of IDs
    # EN/ES
    # Itera a través de cada cadena de IDs
    coins_ids_array.each do |ids|
      # Gets metadata of each string of IDs
      # EN/ES
      # Obtiene metadata de cada cadena de IDs
      response = self.get_metadata(ids, listings)
      # Checks if request was successful
      # EN/ES
      # Revisa que la petición haya sido exitosa
      if response["status"]["error_code"].to_i == 0
        # Iterates through each coin
        # EN/ES
        # Itera a través de cada moneda
        response["data"].each do |cmc_coin|
          # Checks if coin is registered
          # EN/ES
          # Revisa si la moneda está registrada
          coin = Coin.find_by_cmc_id(cmc_coin[0])
          if coin != nil
            # Coin already registered
            # EN/ES
            # Moneda registrada
            coin.update!(
              :name         => cmc_coin[1]["name"],
              :symbol       => cmc_coin[1]["symbol"],
              :slug         => cmc_coin[1]["slug"],
              :logo         => cmc_coin[1]["logo"],
              :website      => cmc_coin[1]["urls"]["website"][0],
              :source_code  => cmc_coin[1]["urls"]["source_code"][0],
              :reddit       => cmc_coin[1]["urls"]["reddit"][0],
              :twitter      => cmc_coin[1]["urls"]["twitter"][0]
            )
          else
            # Coin not registered
            # EN/ES
            # Moneda no registrada
            coin = Coin.create!(
              :cmc_id       => cmc_coin[0],
              :name         => cmc_coin[1]["name"],
              :symbol       => cmc_coin[1]["symbol"],
              :slug         => cmc_coin[1]["slug"],
              :logo         => cmc_coin[1]["logo"],
              :website      => cmc_coin[1]["urls"]["website"][0],
              :source_code  => cmc_coin[1]["urls"]["source_code"][0],
              :reddit       => cmc_coin[1]["urls"]["reddit"][0],
              :twitter      => cmc_coin[1]["urls"]["twitter"][0]
            )
          end
        end
      else
        # Request wasn't successful
        # EN/ES
        # La petición no fue exitosa
        return false
      end
    end
  end

  def self.selectCoinsForPrices(listings, selected_coins)
    coins = []
    ids = ""

    if listings["status"]["error_code"].to_i != 0
      return listings
    end

    listings["data"].each do |listing|
      if selected_coins.length <= 0
        break
      else
        selected_coins.each do |selected_coin|
          if listing["id"] == selected_coin["id"]
            coin = listing
            coin[:metadata] = Coin.find_by_cmc_id(listing["id"])
            coins.push(coin)
            ids += "#{listing['id']},"
            selected_coins.delete(selected_coin)
            break
          end
        end
      end
    end

    return coins
  end

end
