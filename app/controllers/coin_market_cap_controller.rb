class CoinMarketCapController < ApplicationController
  include CmcApi
  skip_before_action    :authorize_request, only: [:listings, :get_metadata, :selected_coins_prices]
  # before_action         :get_listings, only: [:listings, :selected_coins_prices]

  def listings
    @listings = Rails.cache.fetch("listings", expires_in: 1.hour) do
      CmcApi.listings
    end
    json_response(@listings)
  end

  def get_metadata
    @listings = Rails.cache.fetch("listings", expires_in: 1.hour) do
      CmcApi.listings
    end
    @response = CmcApi.get_metadata(params[:id], @listings)
    json_response(@response)
  end

  def selected_coins_prices
    @listings = Rails.cache.fetch("listings", expires_in: 1.hour) do
      CmcApi.listings
    end
    @response = CmcApi.selectCoinsForPrices(@listings, params[:selected_coins])
    json_response(@response)
  end

  private

  def cmc_params
    params.permit(
      :id,
      :selected_coins => []
    )
  end

  def get_listings
    @listings = Rails.cache.fetch("listings", expires_in: 1.hour) do
      CmcApi.listings
    end
  end

end
