# Salsatrackr API

##Espa�ol
Backend del proyecto "Salsatrackr". Esta API es la que sirve la aplicaci�n h�brida, haciendo uso del API de CoinMarketCap (https://coinmarketcap.com/api/)
y guardando algunas cosas en base de datos.
##English
Backend of the project "Salsatrackr". This API serves the hybrid application, using CoinMarketCap's API (https://coinmarketcap.com/api/) and saving some
things in the database.

##Info

###Ruby 2.5.0
###System dependencies
-Ruby 2.5.0

-Rails 5.2.0

-Postgresql

###You will need a CoinMarketCap API KEY (Production and/or testing key)

###Configuration
-Open a terminal window

-Navigate to the project's directory

-Run ```bundle install``` to install the project

-Run ```bundle exec figaro install``` to install Figaro (ENV variables)

-Open ```./config/application.yml``` and type a new variable named ```DEV_API_URL``` which will be used in a local environment. If you plan to upload
this to production, then also add ```PROD_API_URL```

###Database creation
-In a terminal window, type ```rails db:create``` and it will create development and test databases
###Database initialization
-In a terminal window, type ```rails coins:get_metadata``` to get the latest coins listings and download all the metadata to the database.
