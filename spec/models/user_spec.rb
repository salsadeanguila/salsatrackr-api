require 'rails_helper'

RSpec.describe User, type: :model do

  # Ensure all this fields are present before save
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password_digest) }
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:date_of_birth) }

  # Ensure email is unique
  # it { should validate_uniqueness_of(:email) }

end
