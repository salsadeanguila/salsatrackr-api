FactoryBot.define do
  factory :user do
    name { Faker::SiliconValley.character }
    email { Faker::SiliconValley.email }
    password 'password'
    date_of_birth { Date.today }
  end
end
