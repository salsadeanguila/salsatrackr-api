namespace :coins do
  desc "Get latest coins metadata and store it on DB"

  task get_metadata: :environment do
    CmcApi.get_all_metadata(CmcApi.listings)
  end
end
